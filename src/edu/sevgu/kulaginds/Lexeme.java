package edu.sevgu.kulaginds;

public class Lexeme {

    private TokenType type;

    private String data;

    private int line_num;

    Lexeme(TokenType type, int line_num) {
        this(type, null, line_num);
    }

    Lexeme(TokenType type, String data, int line_num) {
        this.type = type;
        this.data = data;
        this.line_num = line_num;
    }

    public TokenType getType() {
        return type;
    }

    public String getData() {
        return data;
    }

    public int getLineNum() {
        return line_num;
    }

    public synchronized String toString() {
        return (data != null)?String.format("%s(%s)", type.toString(), data):type.toString();
    }

}
