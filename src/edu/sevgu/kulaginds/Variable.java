package edu.sevgu.kulaginds;

public class Variable {

    private String name;

    private VariableType type;

    private int data;

    private int line_num;

    Variable(String name, VariableType type, int data, int line_num) {
        this.name = name;
        this.type = type;
        this.data = data;
        this.line_num = line_num;
    }

    public String getName() {
        return name;
    }

    public VariableType getType() {
        return type;
    }

    public int getData() {
        return data;
    }

    public int getLineNum() {
        return line_num;
    }

}
