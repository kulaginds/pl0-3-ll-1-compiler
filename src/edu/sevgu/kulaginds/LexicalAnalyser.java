package edu.sevgu.kulaginds;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class LexicalAnalyser {

    private String file;

    private List<Lexeme> lexemes;
    private CharStack tmp;

    private int identificator_max;
    private int number_max;

    LexicalAnalyser(String input_file, int num_max, int id_max) throws IOException, LexicalAnalyserException {
        lexemes = new LinkedList<>();
        tmp = new CharStack();
        identificator_max = id_max;
        number_max = num_max;

        // open file and clear from unknown symbols and spaces
        loadFile(input_file);

        // generate lexems
        parse();
    }

    public List<Lexeme> getLexemes() {
        return lexemes;
    }

    private void loadFile(String input_file) throws IOException, LexicalAnalyserException {
        BufferedReader br = new BufferedReader(new FileReader(input_file));
        String line;

        while (null != (line = br.readLine())) {
            if (file == null)
                file = line;
            else
                file += '\n' + line;
        }



        if (file == null || (file != null && file.length() == 0))
            throw new LexicalAnalyserException(LexicalAnalyserException.FILE_EMPTY);
    }

    private void parse() throws LexicalAnalyserException {
        int i = 0;
        char c;
        TokenType type = TokenType.NULL;
        String value;
        boolean end_comment = false;
        int line_count = 1;

        while ((i+1) <= file.length()) {
            c = file.charAt(i);

            // is identificator
            if (Character.isLetter(c)) {
                tmp.push(c);
                while ((Character.isLetter(c) || Character.isDigit(c)) && ((i+1) < file.length())) {
                    c = file.charAt(++i);
                    if (Character.isLetter(c)) {
                        tmp.push(c);
                    } else if (Character.isDigit(c)) {
                        tmp.push(c);
                        type = TokenType.IDENTIFICATOR;
                    }
                }

                if ((i+1) == file.length())
                    i++;
                value = tmp.reverseString();

                if (type != TokenType.IDENTIFICATOR) {
                    type = getReservedWordType(value);
                    if (type != TokenType.IDENTIFICATOR)
                        value = null;
                } else if (value.length() > identificator_max)
                    throw new LexicalAnalyserException(LexicalAnalyserException.IDENTIFICATOR_TOO_LONG);

                lexemes.add(new Lexeme(type, value, line_count));
            }
            // is number
            else if (Character.isDigit(c)) {
                type = TokenType.NUMBER;
                tmp.push(c);
                while (Character.isDigit(c) && ((i+1) < file.length())) {
                    c = file.charAt(++i);
                    if (Character.isDigit(c)) {
                        tmp.push(c);
                    }
                }

                if (Character.isLetter(c)) {
                    throw new LexicalAnalyserException(LexicalAnalyserException.IDENTIFICATOR_CANT_START_NUMBER);
                }

                if ((i+1) == file.length())
                    i++;

                value = tmp.reverseString();

                if (value.length() > number_max)
                    throw new LexicalAnalyserException(LexicalAnalyserException.NUMBER_TOO_LONG);

                lexemes.add(new Lexeme(type, value, line_count));
            }
            // is special symbol
            else if (isSpecialSymbol(c)) {
                // check for comment
                if (c == '/') {
                    c = file.charAt(++i);
                    if (c == '*') {
                        while (!end_comment && ((i+1) < file.length())) {
                            c = file.charAt(++i);
                            if (c == '*') {
                                c = file.charAt(++i);
                                if (c == '/') {
                                    end_comment = true;
                                    i++;
                                }
                            }
                        }

                        if (!end_comment)
                            throw new LexicalAnalyserException(LexicalAnalyserException.FILE_END_BEFORE_COMMENT_END);
                    } else
                        c = file.charAt(--i);
                }

                // if we didn't run into a comment
                if (!end_comment) {
                    tmp.push(c);
                    if (isSpecialSymbol(c)) {
                        c = file.charAt(++i);
                        if (isSpecialSymbol(c)) {
                            tmp.push(c);
                        } else
                            c = file.charAt(--i);
                    }/* else {
                        tmp.pop(); i--;
                    }*/

                    // check to make sure t_tmp gets set
                    if (tmp.size() > 0 && type == TokenType.NULL) {
                        type = getSpecialSymbolType(tmp.reverseString());
                        if (type == TokenType.NULL) {
                            i--;
                        } else
                            i++;
                    }

                    if (type != TokenType.NULL)
                        lexemes.add(new Lexeme(type, line_count));
                    else {
                        /*tmp.pop();
                        if (tmp.size() <= 1 && isInvisibleSymbol(tmp.pop()))*/
                            throw new LexicalAnalyserException(LexicalAnalyserException.INVALID_SYMBOL);
                    }
                }
            } else if (!isInvisibleSymbol(c))
                throw new LexicalAnalyserException(LexicalAnalyserException.INVALID_SYMBOL);
            else {
                i++;
                if (c == '\n')
                    line_count++;
            }

            tmp.clear();
            type = TokenType.NULL;
            value = null;
            end_comment = false;
        }
    }

    private TokenType getReservedWordType(String name) {
        switch (name) {
            case "CONST":
                return TokenType.CONST;
            case "VAR":
                return TokenType.VAR;
            case "BEGIN":
                return TokenType.BEGIN;
            case "END":
                return TokenType.END;
            case "IF":
                return TokenType.IF;
            case "THEN":
                return TokenType.THEN;
            default:
                return TokenType.IDENTIFICATOR;
        }
    }

    private boolean isSpecialSymbol(char c) {
        switch (c) {
            case ',':
            case ':':
            case ';':
            case '=':
            case '+':
            case '-':
            case '*':
            case '/':
            case '<':
            case '>':
                return true;
            default:
                return false;
        }
    }

    private TokenType getSpecialSymbolType(String c) {
        switch (c) {
            case "+":
                return TokenType.PLUS;
            case "-":
                return TokenType.MINUS;
            case "*":
                return TokenType.MULTIPLY;
            case "/":
                return TokenType.DIVIDE;
            case "=":
                return TokenType.ASSIGN_CONST;
            case ":=":
                return TokenType.ASSIGN_VAR;
            case "==":
                return TokenType.EQUAL;
            case "<>":
                return TokenType.NOT_EQUAL;
//            case "<":
//                return TokenType.LESS;
//            case ">":
//                return TokenType.MORE;
            case ",":
                return TokenType.COMMA;
            case ";":
                return TokenType.SEMICOLON;
            default:
                return TokenType.NULL;
        }
    }

    private boolean isInvisibleSymbol(char c) {
        switch (c) {
            case 32:
            case '\t':
            case '\n':
            case '\r':
                return true;
            default:
                return false;
        }
    }

}
