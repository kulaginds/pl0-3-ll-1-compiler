package edu.sevgu.kulaginds;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        String input_file = "input.txt";
        int number_max_size = 5;
        int identificator_max_size = 11;

        if (args != null && args.length == 1)
            input_file = args[0];

        LexicalAnalyser la = null;
        SyntaxAnalyser sa = null; // BUG: не видит END после BEGIN и оператора

        try {
            la = new LexicalAnalyser(input_file, number_max_size, identificator_max_size);
            sa = new SyntaxAnalyser(la.getLexemes());
            System.out.println("0 errors");
        } catch (LexicalAnalyserException e) {
            System.err.printf("Lexical error: %s\n", e.getMessage());
        } catch (IOException e) {
            System.err.printf("IO error: %s\n", e.getMessage());
        } catch (SyntaxAnalyserException e) {
            System.err.printf("Syntax error: %s\n", e.getMessage());
        }
    }
}
