package edu.sevgu.kulaginds;

/**
 * Created by Dmitry on 22.04.2016.
 */
public enum VariableType {
    CONSTANT, VARIABLE
}
