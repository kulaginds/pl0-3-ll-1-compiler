package edu.sevgu.kulaginds;

public enum TokenType {
    NULL, CONST, VAR, BEGIN, END, IF, THEN, NUMBER, IDENTIFICATOR,
    COMMA, ASSIGN_CONST, ASSIGN_VAR, SEMICOLON,
    PLUS, MINUS, MULTIPLY, DIVIDE, /*MORE, LESS,*/ EQUAL, NOT_EQUAL
}
