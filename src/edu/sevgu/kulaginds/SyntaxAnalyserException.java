package edu.sevgu.kulaginds;

/**
 * Created by Dmitry on 20.04.2016.
 */
public class SyntaxAnalyserException extends Exception {

    static final String UNEXPECTED_END_OF_FILE = "unexpected end of file";
    static final String TOKEN_EXPECTED = "token expected %s in line %d";
    static final String UNKNOWN_IDENTIFICATOR = "unknown identificator: %s in line %d";

    SyntaxAnalyserException(String msg) {
        super(msg);
    }

}
