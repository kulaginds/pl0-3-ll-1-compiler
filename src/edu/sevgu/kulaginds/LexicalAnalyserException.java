package edu.sevgu.kulaginds;

/**
 * Created by Dmitry on 17.04.2016.
 */
public class LexicalAnalyserException extends Exception {

    static final String FILE_EMPTY = "file empty";
    static final String IDENTIFICATOR_CANT_START_NUMBER = "identificator can't start with digit";
    static final String IDENTIFICATOR_TOO_LONG = "identificator too long";
    static final String NUMBER_TOO_LONG = "number too long";
    static final String FILE_END_BEFORE_COMMENT_END = "file end before comment end";
    static final String INVALID_SYMBOL = "invalid symbol";

    LexicalAnalyserException(String msg) {
        super(msg);
    }
}
