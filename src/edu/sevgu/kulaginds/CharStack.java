package edu.sevgu.kulaginds;

import java.util.Stack;

public class CharStack extends Stack<Character> {

    public String reverseString() {
        char[] str = new char[size()];
        int i = size() - 1;
        while (!empty())
            str[i--] = pop();
        return String.valueOf(str);
    }

}
