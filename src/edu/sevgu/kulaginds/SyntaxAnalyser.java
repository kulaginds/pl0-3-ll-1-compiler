package edu.sevgu.kulaginds;

import java.util.*;

public class SyntaxAnalyser {

    private ListIterator<Lexeme> iterator;
    private Map<String, Variable> table;

    SyntaxAnalyser(List<Lexeme> lexemes_list) throws SyntaxAnalyserException {
        iterator = lexemes_list.listIterator();
        table = new HashMap<>();
        block();
    }

    private Lexeme getNext() throws SyntaxAnalyserException {
        if (!iterator.hasNext())
            throw new SyntaxAnalyserException(SyntaxAnalyserException.UNEXPECTED_END_OF_FILE);
        return iterator.next();
    }

    private Lexeme next(TokenType type) throws SyntaxAnalyserException {
        return next(type, true);
    }

    private Lexeme next(TokenType type, boolean mode_strict) throws SyntaxAnalyserException {
        Lexeme l = getNext();
        if (l.getType() == type)
            return l;
        else
            if (mode_strict)
                throw new SyntaxAnalyserException(String.format(SyntaxAnalyserException.TOKEN_EXPECTED, type.toString(), l.getLineNum()));
            else
                return null;
    }

    private void addVariable(String name, VariableType type, int data, int line_num) {
        try {
            checkVariable(name, line_num);
        } catch (SyntaxAnalyserException e) {
            table.put(name, new Variable(name, type, data, line_num));
        }
    }

    private void checkVariable(String name, int line_num) throws SyntaxAnalyserException {
        if (null == table.get(name))
            throw new SyntaxAnalyserException(String.format(SyntaxAnalyserException.UNKNOWN_IDENTIFICATOR, name, line_num));
    }

    /*
        ГРАММАТИКА
     */

    // <блок>
    private void block() throws SyntaxAnalyserException {
        constants_declaration();
        next(TokenType.SEMICOLON);
        vars_declaration();
        next(TokenType.SEMICOLON);
        operator();
    }

        // <декларация констант>
        private void constants_declaration() throws SyntaxAnalyserException {
            next(TokenType.CONST);
            constants_list();
        }
            // <список определений констант>
            private void constants_list() throws SyntaxAnalyserException {
                constant();
                x1();
            }

            // <определение константы>
            private void constant() throws SyntaxAnalyserException {
                Lexeme const_name,
                       const_value;
                const_name = next(TokenType.IDENTIFICATOR);
                next(TokenType.ASSIGN_CONST);
                const_value = next(TokenType.NUMBER);

                addVariable(const_name.getData(), VariableType.CONSTANT, Integer.parseInt(const_value.getData()), const_name.getLineNum());
            }

            // <x1>
            private void x1() throws SyntaxAnalyserException {
                Lexeme c = next(TokenType.COMMA, false);
                if (null != c)
                    constants_list();
                else
                    iterator.previous();
            }

        // <декларация переменных>
        private void vars_declaration() throws SyntaxAnalyserException {
            next(TokenType.VAR);
            vars_list();
        }

            // <список переменных>
            private void vars_list() throws SyntaxAnalyserException {
                Lexeme var_name = next(TokenType.IDENTIFICATOR);
                addVariable(var_name.getData(), VariableType.VARIABLE, 0, var_name.getLineNum());
                x2();
            }

            // <x2>
            private void x2() throws SyntaxAnalyserException {
                Lexeme c = next(TokenType.COMMA, false);
                if (c != null)
                    vars_list();
                else
                    iterator.previous();
            }

    // <список операторов>
    private void operators_list() throws SyntaxAnalyserException {
        operator();
        x3();
    }

        // <оператор>
        private void operator() throws SyntaxAnalyserException {
            Lexeme identificator_name;
            identificator_name = getNext();
            switch (identificator_name.getType()) {
                case IDENTIFICATOR:
                    next(TokenType.ASSIGN_VAR);
                    expression();
                    // TODO: куда-то добавить identificator_name
                    break;
                case BEGIN:
                    operators_list();
                    next(TokenType.END);
                    break;
                case IF:
                    // TODO: condition тоже нужно куда-то добавить
                    condition();
                    next(TokenType.THEN);
                    operator();
                    break;
                default:
                    throw new SyntaxAnalyserException(String.format(SyntaxAnalyserException.TOKEN_EXPECTED, identificator_name.getType().toString(), identificator_name.getLineNum()));
            }
        }

            // <выражение>
            private void expression() throws SyntaxAnalyserException {
                term();
                x5();
            }

                // <слагаемое>
                private void term() throws SyntaxAnalyserException {
                    multiplier();
                    x6();
                }

                    // <множитель
                    private void multiplier() throws SyntaxAnalyserException {
                        Lexeme c = getNext();
                        switch (c.getType()) {
                            case IDENTIFICATOR:
                                // TODO: добавляет куда-то c == identificator
                                checkVariable(c.getData(), c.getLineNum());
                                break;
                            case NUMBER:
                                // TODO: добавляет куда-то c == number
                                break;
                            default:
                                iterator.previous();
                                expression();
                                break;
                        }
                    }

                    // <x6>
                    private void x6() throws SyntaxAnalyserException {
                        Lexeme c = getNext();

                        switch (c.getType()) {
                            case MULTIPLY:
                            case DIVIDE:
                                // TODO: добавить куда-то знак
                                multiplier();
                                x6();
                                break;
                            default:
                                iterator.previous();
                                break;
                        }
                    }

                // <x5>
                private void x5() throws SyntaxAnalyserException {
                    Lexeme c = getNext();

                    switch (c.getType()) {
                        case PLUS:
                        case MINUS:
                            // TODO: добавить куда-то знак
                            term();
                            x5();
                            break;
                        default:
                            iterator.previous();
                            break;
                    }
                }

            // <условие>
            private void condition() throws SyntaxAnalyserException {
                expression();
                x4();
            }

            // <x4>
            private void x4() throws SyntaxAnalyserException {
                Lexeme c = getNext(),
                       expr;

                switch (c.getType()) {
                    case EQUAL:
                    case NOT_EQUAL:
                        // TODO: добавляет тип сравнения куда-то
                        expression();
                        break;
                    default:
                        iterator.previous();
                        break;
                }
            }

        // <x3>
        private void x3() throws SyntaxAnalyserException {
            Lexeme c = next(TokenType.SEMICOLON, false);

            if (c != null)
                operators_list();
            else
                iterator.previous();
        }

}
